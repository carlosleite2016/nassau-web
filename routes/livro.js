const express = require('express');
const router = express.Router();
const listaDeLivros = require('./../seed/livros_seed');

router.get('/', (req, res) => {
  res.render('livros/index', { livros: listaDeLivros });
});

router.get('/novo', (req, res) => {
  res.render('livros/novo');
});

router.post('/novo', (req, res) => {
  
  if (req.body.isbn && req.body.titulo && req.body.edicao){
  listaDeLivros.push({
    
    isbn: req.body.isbn,
    titulo: req.body.titulo,
    edicao: req.body.edicao
       
  });
  }   
  
  res.redirect('/livros');
  
});

router.get('/:isbn/delete', (req, res) => {
  
   var posicao = listaDeLivros.map((livro) => {
     return livro.isbn;
   }).indexOf(req.params.isbn);
  
  listaDeLivros.splice(posicao, 1);
  
  res.redirect('/livros');
});


module.exports = router;
