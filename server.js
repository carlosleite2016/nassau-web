const express = require("express");
const app     = express();
const port    = process.env.PORT || 8080;
const hbs     = require('hbs');
const bodyparser = require('body-parser');    
const livroRouters = require('./routes/livro');

hbs.registerPartials(__dirname + '/views/partials');

app.use(bodyparser.urlencoded({extended: true}));

app.use(express.static(__dirname + '/public'));

app.set("view engine", "hbs");

app.use('/livros', livroRouters);

app.get('/', (req, res) => {   
  res.render("index");
});



//app.send('/livros', (req, res) => {
//  res.send('Livros');
//});

app.listen(port, () => {
  console.log(`The server ir running on port ${port}.`);
});
